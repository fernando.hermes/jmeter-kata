## BUILDER
FROM gradle:6.9.1-jdk8 AS builder
USER root
COPY src src
COPY build.gradle build.gradle
COPY settings.gradle settings.gradle
COPY dependencies.gradle.kts dependencies.gradle.kts
RUN gradle test installDist
COPY src/main/resources/java.security  /home/gradle/build/install/jmeter-kata/bin/java.security

# RUNNER
FROM openjdk:8u212-jdk-alpine
WORKDIR /home/api
EXPOSE 8080
COPY --from=builder /home/gradle/build/install/jmeter-kata  /home/api
RUN chmod +x /home/api/bin/jmeter-kata

ENTRYPOINT ["/home/api/bin/jmeter-kata"]
