# JMeter Kata

Apache JMeter is an application made in Java designed to load test functional behavior and measure performance.
It was originally designed for testing Web Applications but has since expanded to other test functions.

It may be used to test performance both on static and dynamic resources, Web dynamic applications.
It can be used to simulate a heavy load on a server, group of servers, network or object to test its strength
or to analyze overall performance under different load types

------

## Installation

### A) Download path

1. Download JMeter from:
    - https://dlcdn.apache.org//jmeter/binaries/apache-jmeter-5.5.zip
2. Open `bin/jmeter.sh` using Finder or executing `sh jmeter.sh` in the terminal

### B) Brew path

1. Install JMeter executing the following command:
    - brew install jmeter
2. Execute `jmeter` in the terminal

------

## Practice

### Exercise 1

First off, we need to add a **Thread Group**

- To do this, right click over `Test Plan` and go to `Add -> Threads -> Thread Group`

<img src="kata-resources/readme-resources/exercise-1/1-add-thread-group.png" width="800"/>

- The **Thread Group** will determine how many requests are executed, and how many threads/users
  should execute the requests in parallel

Next, add a **HTTP Request Sampler**

- Right click over the `Thread Group` and go to `Add -> Sampler -> HTTP Request`

<img src="kata-resources/readme-resources/exercise-1/2-add-sampler-http-request.png" width="800"/>

Next we need to configure our **Thread Group**.

For this exercise we will simulate only 1 user executing 1 request.

To do this, step over the `Thread Group` and configure the following:

- `Number of Threads` : 1
- `Loop Count` : 1

![](kata-resources/readme-resources/exercise-1/3-configure-thread-group.png)

Finally, we have to configure our **HTTP Request Sampler**

Step over it and configure the following values:

- Web Server section
    - `Server Name or IP` : localhost
    - `Port` : 8080
- HTTP Request section
    - Select `POST` in the dropdown
    - `Path` : `/process`

![](kata-resources/readme-resources/exercise-1/4-configure-http-request.png)

Go to `Main.kt` file and start the API

Go back to JMeter and over the `Thread Pool` do `Right click -> Start`

<img src="kata-resources/readme-resources/exercise-1/5-start-the-thread-pool.png" width="500"/>

And it's done! You executed your first request using JMeter

... But as it is configured now there isn't much difference between this and executing the request using `curl`
or `Postman`

-------

### Exercise 2

Let's start making stress tests

Duplicate the previous **Thread Group** doing `Right click -> Duplicate` over it

Change the `Loop Count` to 100

Start the `Thread Group`

You'll see the requests arriving to the API, and logs of them being processed

But as it is, you can't check anything in JMeter

To change this, step over the `Thread Group` and do `Right click -> Add -> Listener -> View Results Tree`

Start the `Thread Group` again

You'll see the requests being listed in the `View Results Tree`

There you can step over one of the requests and check it information, like

- Request Body
- Request Headers
- Response data
- Response code

You can clear the `View Results Tree` executing `Right click -> Clear` over it

Let's add some colors in it

Go to the `HTTP Request` and change the `Path` to `/random-error-process`

Start the `Thread Group` again

Now if you check the `View Results Tree` you'll see that some requests are returning with errors.

As with the ones that return `200 OK`, you can check the information of the failing requests and what happened if you
step over them and check the information provided

-----

### Exercise 3

As you have it configured now, you can check if the requests are being returned OK or with errors, but as you execute
more requests the list gets bigger and the visibility of what is happening overall starts to become more difficult to grasp

Let's change that!

Duplicate the last `Thread Group`

While over it, add another listener doing `Right click -> Add -> Listener -> Summary Report`

Start the `Thread Group` and step over the `Summary Report`

As the requests get executed OK or with errors, you'll see the error rate (`Error %` column) change

You can check the `Throughput` too

This is a nice way to check those values, but let's make it more visually appealing

------

### Exercise 4

Duplicate the last `Thread Group`

Change the `Number of Threads` to 5 and the `Loop Count` to infinite (checking the checkbox)

While over it, add another listener doing `Right click -> Add -> Listener -> Graph Results`

Go to the `HTTP Request` and change the `Path` to `/delayed-process`

Start the `Thread Group` and step over the `Graph Results` listener

Uncheck `Median`, `Deviation` and `Throughput`

You'll see dots being painted over the graph, those are the independent requests and their reponse time

Beside the dots, you'll see a blue line being painted, this is the average response time of all of them

This is a nice way of checking the response time of a service while under stress

------

### Exercise 5

Another thing you can do with JMeter is execute requests with payloads (even dynamically generated)

Let's do that!

Create a new `Thread Group` with:

- `Number of threads` : 1
- `Loop count` : 100

Add a `HTTP Header Manager` to it, to do this step over it and
do `Right click -> Add -> Config Element -> HTTP Header Manager`

Step over it and go to `Add` at the bottom of the screen, a line will be added, set the following:

- `Name` : `Content-Type`
- `Value` : `application/json`

Add a `HTTP Request` sampler to the `Thread Group` (or copy one of the other ones)

In it, change the `Path` to `/parameterized-process`

Go to the `Body Data` tab and set the following:

```json
{
  "userId": "${userId}",
  "parameter": "${parameter}"
}
```

This body will be filled with those values, now we need to configure from where it is going to retrieve them

To do this, step over the `HTTP Request` sampler and do `Right click -> Add -> Config Element -> CSV Data Set Config`

In the `CSV Data Set Config` configure the following:

- `Filename` : browse to the `parameterized-process.csv` file inside the `kata-resources` folder in this project
- `Variable Names` : `userId,parameter`

Step over the `Thread Group` and add a `View Results Tree` listener

Lastly, start the `Thread Group`

You'll see the request being populated with the values of the `csv`, you can check how the requests where generated in
the `View Results Tree` stepping on the request you want to check and going to the tab `Request -> Request Body`. You can check the request payload at the API logs too

------

And that's it! There are many more things that can be done with JMeter, but the ones listed here are the basic ones that
can be used to execute and analyze an API robustness
