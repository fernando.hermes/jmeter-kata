package com.etermax.katas.jmeter.http.handler

import io.ktor.application.Application

/**
 * Implementers handle requests in the configured routes by `routing` methods
 */
interface Handler {
	fun routing(a: Application)
}
