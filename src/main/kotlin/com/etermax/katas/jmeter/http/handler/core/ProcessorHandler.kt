package com.etermax.katas.jmeter.http.handler.core

import com.etermax.katas.jmeter.http.handler.Handler
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.util.pipeline.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.serialization.Serializable
import org.slf4j.LoggerFactory
import java.lang.Thread.sleep
import java.util.*
import kotlin.random.Random

class ProcessorHandler : Handler {

	private val logger = LoggerFactory.getLogger(this::class.java)

	override fun routing(a: Application) {
		a.routing {
			route("/process") {
				post { process() }
			}

			route("/delayed-process") {
				post { delayedProcess() }
			}

			route("/random-error-process") {
				post { randomErrorProcess() }
			}

			route("/parameterized-process") {
				post { parameterizedProcess() }
			}
		}
	}

	private suspend fun PipelineContext<Unit, ApplicationCall>.process() {
		val processId = UUID.randomUUID().toString()
		val message = "Request processed. Id: $processId"
		logger.info(message)
		call.respond(message)
	}

	private suspend fun PipelineContext<Unit, ApplicationCall>.delayedProcess() {
        withContext(Dispatchers.IO) {
            val processId = UUID.randomUUID().toString()
            val sleepTime = Random.nextInt(1, 10) * 250L
            logger.info("sleeping for $sleepTime millis...")
            sleep(sleepTime)
            val message = "Request processed with delay. Id: $processId"
            logger.info(message)
            call.respond(message)
        }
    }

	private suspend fun PipelineContext<Unit, ApplicationCall>.randomErrorProcess() {
		val processId = UUID.randomUUID().toString()
		sleep(1000)
		val randomError = Random.nextInt(0, 10)
		if(randomError < 3) {
			val message = "Request couldn't be processed. Id: $processId"
			logger.error(message)
			call.respond(HttpStatusCode.InternalServerError, message)
		} else {
			val message = "Request processed successfully. Id: $processId"
			logger.info(message)
			call.respond(message)
		}
	}


	private suspend fun PipelineContext<Unit, ApplicationCall>.parameterizedProcess() {
		val processId = UUID.randomUUID().toString()
		val request = call.receive<ParameterizedProcessRequest>()
		val message = "Request processed with parameters [ userId: ${request.userId} - parameter: ${request.parameter} ]. Id: $processId"
		logger.info(message)
		call.respond(message)
	}

	@Serializable
	data class ParameterizedProcessRequest (val userId : String, val parameter: String)


}
