package com.etermax.katas.jmeter

import com.etermax.katas.jmeter.modules.Banner
import com.etermax.katas.jmeter.modules.DeliveryProvider.apiServer
import org.slf4j.Logger
import org.slf4j.LoggerFactory

fun main() {
    val logger: Logger = LoggerFactory.getLogger("Main")

    println(Banner.TEXT)

    logger.info("Starting main application ...")

    apiServer.start()
}
