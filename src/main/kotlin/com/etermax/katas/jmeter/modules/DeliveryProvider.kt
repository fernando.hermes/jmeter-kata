package com.etermax.katas.jmeter.modules

import com.etermax.katas.jmeter.http.HttpApiServer
import com.etermax.katas.jmeter.http.handler.core.ProcessorHandler

import com.etermax.katas.jmeter.modules.ConfigurationProvider.config

object DeliveryProvider {

    val apiServer by lazy {
        HttpApiServer(
            config.app,
            processorHandler,
        )
    }

    val processorHandler by lazy {
        ProcessorHandler()
    }
}
