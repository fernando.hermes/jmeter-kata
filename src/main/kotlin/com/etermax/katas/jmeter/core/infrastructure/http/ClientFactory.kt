package com.etermax.katas.jmeter.core.infrastructure.http

import com.etermax.ktor.plugins.metrics.PrometheusClient
import io.ktor.client.HttpClient
import io.ktor.client.engine.HttpClientEngine
import io.ktor.client.features.HttpTimeout
import io.ktor.client.features.defaultRequest
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.serializer.KotlinxSerializer
import io.ktor.http.Url
import kotlinx.serialization.json.Json

object ClientFactory {

	data class Config(
		val baseUrl: String,
		val defaultConnectTimeoutMs: Long = 500,
		val defaultRequestTimeoutMs: Long = 2000
	) {
		fun url(): Url = Url(baseUrl)
	}

	fun makeClient(config: Config, engine: HttpClientEngine) =
		makeClient(
			config.url(),
			engine,
			config.defaultConnectTimeoutMs,
			config.defaultRequestTimeoutMs
		)

	private fun makeClient(
		targetBaseUrl: Url,
		engine: HttpClientEngine,
		defaultConnectTimeoutMs: Long = 500,
		defaultRequestTimeoutMs: Long = 2000
	): HttpClient {

		return HttpClient(engine) {
			install(HttpTimeout) {
				connectTimeoutMillis = defaultConnectTimeoutMs
				requestTimeoutMillis = defaultRequestTimeoutMs
			}

			install(JsonFeature) {
				serializer = makeJsonSerializer()
			}

			install(PrometheusClient)

			defaultRequest {
				url {
					protocol = targetBaseUrl.protocol
					host = targetBaseUrl.host
					port = targetBaseUrl.port
				}
			}
		}
	}

	private fun makeJsonSerializer() =
		KotlinxSerializer(Json {
			this.ignoreUnknownKeys = true
		})
}
