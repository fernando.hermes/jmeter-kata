package com.etermax.katas.jmeter.modules

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class ConfigurationProviderTest {
    @Test
    fun `should parse YAML configuration without exceptions`() {

        val config = ConfigurationProvider.config

        assertNotNull(config.app.image)
    }
}
