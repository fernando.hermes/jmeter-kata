package com.etermax.katas.jmeter.acceptance

import com.etermax.katas.jmeter.http.handler.core.CounterHandler
import com.etermax.katas.jmeter.modules.ActionsProvider
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.serialization.*
import io.ktor.server.testing.*
import kotlinx.serialization.json.Json

open class AcceptanceSpec {

    private lateinit var path: String
    private lateinit var httpMethod: HttpMethod
    private var contentType: ContentType? = ContentType.Application.Json

    fun givenEndpointWith(path: String, httpMethod: HttpMethod, contentType: ContentType?){
        this.path = path
        this.httpMethod = httpMethod
        this.contentType = contentType
    }

    fun runTest(): TestApplicationResponse {
        return withTestApplication(Application::testMain) {
            with(handleRequest(httpMethod, path){
                addHeader(HttpHeaders.ContentType, contentType.toString())
            }){
                return@with response
            }
        }
    }

    fun then(then: ()->Unit){
        then.invoke()
    }

}

fun Application.testMain() {

    install(DefaultHeaders)
    install(Compression)
    installContentNegotiation()
    install(XForwardedHeaderSupport)
    install(StatusPages) { addExceptionHandlers() }

    routing {
        // TODO find a way to dont redefine the handlers here.
        CounterHandler(ActionsProvider.createNewCounter, ActionsProvider.getCounter, ActionsProvider.increaseCounter)
            .routing(this.application)
    }
}

private fun StatusPages.Configuration.addExceptionHandlers() {
    exception<Exception> { cause ->
        call.respond(HttpStatusCode.InternalServerError, cause::class.java.simpleName + ": " + cause.message)
    }
}

private fun Application.installContentNegotiation() {
    install(ContentNegotiation) {
        json(
            Json {
                ignoreUnknownKeys = true
            }
        )
    }
}
