package com.etermax.katas.jmeter.acceptance

import io.ktor.http.*
import io.ktor.server.testing.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class CounterAcceptanceSpec : AcceptanceSpec() {

    private val USER = "1"
    private val GAME = "TCL"

    @Test
    fun `create counter with success`() {
        givenEndpointWith(
            path = "/games/$GAME/users/$USER/jmeter-kata/counter",
            httpMethod = HttpMethod.Post,
            contentType = ContentType.Application.Json
        )
        givenSomething()

        val response = runTest()

        then { validateOKResponse(response) }

    }

    private fun givenSomething(){
        //... add other pre-conditions here
    }

    private fun validateOKResponse(response: TestApplicationResponse) {
        assertThat(response.status()).isEqualTo(HttpStatusCode.OK)
        assertThat(response.content).contains(""""id":"""")
    }

}



