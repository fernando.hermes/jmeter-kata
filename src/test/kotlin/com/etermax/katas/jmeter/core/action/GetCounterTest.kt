package com.etermax.katas.jmeter.core.action

import com.etermax.katas.jmeter.core.domain.Counter
import com.etermax.katas.jmeter.core.domain.CounterNotFoundException
import com.etermax.katas.jmeter.core.infrastructure.repository.InMemoryCounters
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.Test

class GetCounterTest {

	private val userId = 1L
	private val counterId = "counterId"

	@Test
	fun `A created counter can be retrieved`(): Unit = runBlocking {
		val counters = InMemoryCounters()
		counters.save(userId, Counter(counterId, "AR"))

		val getCounter = GetCounter(counters)
		val counter = getCounter(userId, counterId)

		assertThat(counter).isNotNull
		assertThat(counter.id).isEqualTo(counterId)
	}

	@Test
	fun `A nonexistent counter throws an exception`() {
		val counters = InMemoryCounters()
		val getCounter = GetCounter(counters)

		assertThatThrownBy {
			runBlocking {  getCounter(userId, counterId) }
		}.isInstanceOf(CounterNotFoundException::class.java)
	}
}
