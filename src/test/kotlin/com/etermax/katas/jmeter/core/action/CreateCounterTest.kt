package com.etermax.katas.jmeter.core.action

import com.etermax.katas.jmeter.core.domain.CounterOriginRetriever
import com.etermax.katas.jmeter.core.domain.Counters
import com.etermax.katas.jmeter.modules.MetricsProvider
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test

internal class CreateCounterTest {

    @Test
    fun `should call corresponding creator for mode`() {
        val counters: Counters = mockk()

        coEvery { counters.save(any(), any()) } returns true

        val createGame = CreateCounter(counters, MetricsProvider.counterMetricsReporter, object :CounterOriginRetriever {
			override suspend fun retrieveCountry(ip: String) = "AR"
		})

		runBlocking {
			createGame.invoke(42, "127.0.0.1")
		}

		coVerify(exactly = 1) { counters.save(any(), any()) }
    }
}