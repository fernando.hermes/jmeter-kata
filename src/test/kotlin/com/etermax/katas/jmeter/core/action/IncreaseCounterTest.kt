package com.etermax.katas.jmeter.core.action

import com.etermax.katas.jmeter.core.domain.Counter
import com.etermax.katas.jmeter.core.infrastructure.repository.InMemoryCounters
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class IncreaseCounterTest {

	private val userId = 1L
	private val counterId = "counterId"

	@Test
	fun `A counter can be increased`(): Unit = runBlocking {
		val counters = InMemoryCounters()
		counters.save(userId, Counter(counterId, "AR"))

		val increaseCounter = IncreaseCounter(counters)
		increaseCounter(userId, counterId)

		val counter = counters.find(userId, counterId)
		assertThat(counter).isNotNull
		assertThat(counter?.points).isEqualTo(1)
	}
}