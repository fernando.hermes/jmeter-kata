package com.etermax.katas.jmeter.core.infrastructure.repository

import com.etermax.katas.jmeter.core.domain.Counter
import com.etermax.katas.jmeter.core.domain.Counters
import io.lettuce.core.RedisClient
import io.lettuce.core.api.coroutines
import io.lettuce.core.api.coroutines.RedisCoroutinesCommands
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import redis.embedded.RedisServer


class RedisCountersTest {

	@BeforeEach
	fun before(): Unit = runBlocking {
		redisConnection.flushall()
	}

	private val counterId = "3132"
	private val userId: Long = 1

	@Test
	fun `A counter is saved`(): Unit = runBlocking {
		val counters = givenARepository()
		val counter = givenACounter()

		whenCounterIsPersisted(counters, counter)

		thenCounterCanBeRetrieved(counters)
	}

	@Test
	fun `A non saved counter can not be found`(): Unit = runBlocking {
		val counters = givenARepository()

		val savedCounter = counters.find(userId, counterId)

		assertThat(savedCounter).isNull()
	}

	@Test
	fun `A saved counter can be updated`(): Unit = runBlocking {
		val counters = givenARepository()
		givenASavedCounter(counters)

		whenPointsAreUpdated(counters)

		thenUpdateIsSuccessful(counters)
	}

	private suspend fun givenASavedCounter(counters: RedisCounters) {
		val counter = givenACounter()
		counters.save(userId, counter)
	}

	private suspend fun thenUpdateIsSuccessful(counters: RedisCounters) {
		val updatedCounter = counters.find(userId, counterId)
		assertThat(updatedCounter).isNotNull
		assertThat(updatedCounter?.points).isEqualTo(2)
	}

	private suspend fun whenPointsAreUpdated(counters: RedisCounters) {
		val savedCounter = counters.find(userId, counterId)!!
		savedCounter.increasePoints()
		counters.update(userId, savedCounter)
	}

	private suspend fun thenCounterCanBeRetrieved(counters: RedisCounters) {
		val savedCounter = counters.find(userId, counterId)
		assertThat(savedCounter).isNotNull
		assertThat(savedCounter?.id).isEqualTo(counterId)
	}

	private suspend fun whenCounterIsPersisted(
		counters: RedisCounters,
		counter: Counter
	) {
		counters.save(userId, counter)
	}

	private fun givenACounter(): Counter {
		val counter = Counter(id = counterId, countryCode = "AR")
		counter.increasePoints()
		return counter
	}

	private fun givenARepository() = RedisCounters(redisConnection)

	companion object {

		private lateinit var redisServer: RedisServer
		private lateinit var redisClient: RedisClient
		private lateinit var redisConnection: RedisCoroutinesCommands<String, String>

		@BeforeAll
		@JvmStatic
		fun setup() {
			redisServer = RedisServer.builder().build()
			redisServer.start()
			redisClient = RedisClient.create("redis://localhost:${redisServer.ports()!![0]}")
			redisConnection = redisClient.connect().coroutines()
		}

		@AfterAll
		@JvmStatic
		fun tearDown() {
			redisClient.shutdown()
			redisServer.stop()
		}
	}
}



