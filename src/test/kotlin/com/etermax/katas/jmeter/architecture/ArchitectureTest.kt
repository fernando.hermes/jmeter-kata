package com.etermax.katas.jmeter.architecture

import com.etermax.katas.jmeter.http.handler.Handler
import com.tngtech.archunit.core.domain.JavaClasses
import com.tngtech.archunit.core.importer.ClassFileImporter
import com.tngtech.archunit.core.importer.ImportOption
import com.tngtech.archunit.lang.ArchRule
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes
import com.tngtech.archunit.library.dependencies.SlicesRuleDefinition.slices
import org.junit.jupiter.api.Test

/**
 * ArchitectureTest performs tests against the project architectural rules to find violations
 */
class ArchitectureTest {

	private val importedClasses: JavaClasses = ClassFileImporter()
		.withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
		.importPackages("com.etermax.katas.jmeter")

	lateinit var rule: ArchRule

	@Test
	fun `Project has cycle dependencies`() {
		rule = slices().matching("com.etermax.katas.jmeter.(*)..").should().beFreeOfCycles()
		rule.check(importedClasses)
	}

	@Test
	fun `Every ktor handler ends with Handler`() {
		rule = classes().that().implement(Handler::class.java).should().haveSimpleNameEndingWith("Handler")
		rule.check(importedClasses)
	}

	@Test
	fun `Domain should only use classes from the domain`() {
		rule = classes().that().resideInAnyPackage("..domain..")
			.should().onlyDependOnClassesThat().resideInAnyPackage(
				"..domain..",
				"java..",
				"kotlin..",
				"org.slf4j..",
				"org.jetbrains.annotations"
			)

		rule.check(importedClasses)
	}
}
